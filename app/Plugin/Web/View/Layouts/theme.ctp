<!DOCTYPE html>

<html ng-app="fenapaesApp">
	<head>
		<meta charset="utf-8">
		<title>Fenapaes</title>
		<!-- jQuery -->
		<script src="/web/bower_components/jquery/dist/jquery.js"></script>
		<style>
			/*
			.table td {
				vertical-align: middle !important;
				font-size: 15px !important;
			}
			*/
			.affix {
				z-index: 1000;
				top: 0px;
				left: 15px;
				right: 15px;
			}
		</style>
		<!-- Tweeter Bootstrap -->
		<link rel="stylesheet" href="/web/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" id="BootswatchTheme" href="/web/bower_components/bootswatch/lumen/bootstrap.min.css">
		<link rel="stylesheet" href="/web/bower_components/smartmenus/dist/css/sm-core-css.css">
		<link rel="stylesheet" href="/web/bower_components/smartmenus/dist/addons/bootstrap/jquery.smartmenus.bootstrap.css">

		<style id="BootswatchThemeStyle">
		</style>
		<script src="/web/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- Font Awesome -->
		<link rel="stylesheet" href="/web/bower_components/fontawesome/css/font-awesome.min.css">
		<!-- Angular Js -->
		<script src="/web/bower_components/angular/angular.min.js"></script>
		<script src="/web/bower_components/angular-i18n/angular-locale_pt-br.js"></script>
		
		<script src="/web/bower_components/angular-route/angular-route.min.js"></script>
		<script src="/web/bower_components/angular-resource/angular-resource.min.js"></script>
		<script src="/web/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
		<script src="/web/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
		<script src="/web/bower_components/smartmenus/dist/jquery.smartmenus.min.js"></script>
		<script src="/web/bower_components/smartmenus/dist/addons/bootstrap/jquery.smartmenus.bootstrap.min.js"></script>
		
		<!-- Locais -->
		<script src="/web/components/classy/angular-classy.min.js"></script>
		<script src="/web/components/dialog/dialogs.min.js"></script>
		<script src="/web/components/dialog/dialogs-default-translations.min.js"></script>
		<link href="/web/components/dialog/dialogs.min.css" rel="stylesheet">

		<script src="/web/app.js"></script>
		<script src="/web/mainCtrl.js"></script>
		<script src="/web/app/menu/controllers.js"></script>
		<script src="/web/Data.js"></script>
		<script src="/web/routes.js"></script>
		
		<script src="/web/components/dialog/config.js"></script>

		<!-- Menus -->
		<script src="/web/app/menu/controllers.js"></script>
		<!-- All Controllers -->
		<?php echo $this->Element('controllers'); ?>
		
	</head>
	<body style="margin-top:10px;">
		<?php echo $this->fetch('content'); ?>
	</body>
</html>
