<?php

class Noticia extends WebAppModel {

	public $useTable = 'noticia';
	public $primaryKey = 'not_id';
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['Noticia']['not_data']) ) {
				$results[$key]['Noticia']['not_data'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['Noticia']['not_data'] ) );
			}
		}
		}
		return $results;
	}

}