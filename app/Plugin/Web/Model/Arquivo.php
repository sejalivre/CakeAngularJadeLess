<?php

class Arquivo extends WebAppModel {

	public $useTable = 'arquivo';
	public $primaryKey = 'arq_id';
	
	public $belongsTo = array(
		'ArquivoTop' => array(
			'className' => 'Web.ArquivoTop',
			'foreignKey' => 'arq_art_id'
		)
	);
	
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['Arquivo']['arq_data']) ) {
				$results[$key]['Arquivo']['arq_data'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['Arquivo']['arq_data'] ) );
			}
		}
		}
		return $results;
	}
	

}