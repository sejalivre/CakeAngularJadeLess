<?php

class Assunto extends AdminAppModel {

	public $useTable = 'assuntos';
	
	public $hasMany = array(
		'Chamada' => array(
			'className' => 'Admin.Chamada'
		)
	);
	
}