<?php

class ArquivoTop extends WebAppModel {

	public $useTable = 'arquivo_top';
	public $primaryKey = 'art_id';
	
	public $hasMany = array(
		'Arquivo' => array(
			'className' => 'Web.Arquivo',
			'foreignKey' => 'arq_art_id'
		)
	);

}