<?php

class Entidade extends WebAppModel {

	public $useTable = 'entidade';
	public $primaryKey = 'ent_id';
	public $useDbConfig = 'Apaebrasil';
	
	public $hasMany = array(
		'Menu1' => array(
			'className' => 'Web.Menu1',
			'foreignKey' => 'men_ent_id'
		)
	);

	
}