<?php

class BannerImg extends WebAppModel {

	public $useTable = 'banner_img';
	public $primaryKey = 'bai_id';
	
	public $belongsTo = array(
		'Banner' => array(
			'className' => 'Web.Banner'
		)
	);
}