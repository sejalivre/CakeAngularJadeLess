fenapaesApp.cC({
	name: 'artigoCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Artigo = this.$resource('/api/artigo/:id.json');
		this.$.paginator = {
			page: 1
		};
		this._load();
	},
	watch: {
		'{object}paginator.page':'_load'
	},
	methods: {
		_load: function() {
			
			this.Artigo.get(
				{
					q: 'Artigo.sec_ent_id.eq.1',
					page: this.$.paginator.page
				}
			).$promise.
			then(function(data){
				this.$.artigos = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
		}
	}
});

fenapaesApp.cC({
	name: 'artigoViewCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Artigo = this.$resource('/api/artigo/:id.json');
		this._view();
	},
	watch: {
		'{object}artigo':'_handleLinks()'
	},
	methods: {
		_handleLinks: function() {
			arq_links = $('a[href*="arquivo.phtml?a="]');
			arq_links.each(function(link){
				href = $(arq_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/arquivo/'+href[1];
				$(arq_links[link]).attr('href',nhref);
			});

			art_links = $('a[href*="artigo.phtml?a="]');

			art_links.each(function(link){
				href = $(art_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/artigo/'+href[1];
				$(art_links[link]).attr('href',nhref);
			});
			
			
			art_links = $('a[href*="artigo.phtml/"]');
			
			art_links.each(function(link){
				href = $(art_links[link]).attr('href');
				href = href.split('/');
				nhref = '#/artigo/'+href[href.length-1];
				$(art_links[link]).attr('href',nhref);
			});
		},
		_view: function() {
			this.Artigo.get(
				{
					id: this.$routeParams.id
				}
			).$promise.
			then(function(data){
				this.$.artigo = data.data;
				setTimeout(function(){
					this._handleLinks();
				}.bind(this), 1000);
			}.bind(this));
		}
	}
});
