fenapaesApp.cC({
	name: 'homeCtrl', 
	inject: ['$scope','$resource','dialogs'],
	init: function() {
		this.$.slides = [
			{
				image: '/web/img/banner_1.jpg'
			},
			{
				image: '/web/img/banner_2.jpg'
			},
			{
				image: '/web/img/banner_3.jpg'
			},
			{
				image: '/web/img/banner_4.jpg'
			},
			{
				image: '/web/img/banner_5.jpg'
			},
			{
				image: '/web/img/banner_6.jpg'
			}
		];
		
		this.Noticia = this.$resource('/api/noticia/:id.json');
		
		this._destques();
	},
	methods: {
		_destques: function() {
			this.Noticia.get(
				{
					q: 'Noticia.not_destaque.eq.1,Noticia.not_ent_id.eq.1',
					limit: 8,
					order: '-not_data'
				}
			).$promise.
			then(function(data){
				this.$.destaques = data.data;
			}.bind(this));
		}
	}
});
