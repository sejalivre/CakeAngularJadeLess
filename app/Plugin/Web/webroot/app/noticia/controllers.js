fenapaesApp.cC({
	name: 'noticiaCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Noticia = this.$resource('/api/noticia/:id.json');
		this.$.paginator = {
			page: 1
		};
		this._load();
	},
	watch: {
		'{object}paginator.page':'_load'
	},
	methods: {
		_load: function() {
			
			this.Noticia.get(
				{
					q: 'Noticia.not_ent_id.eq.1',
					sort: '-not_data',
					page: this.$.paginator.page
				}
			).$promise.
			then(function(data){
				this.$.noticias = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
		}
	}
});

fenapaesApp.cC({
	name: 'noticiaViewCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Noticia = this.$resource('/api/noticia/:id.json');
		this._view();
	},
	methods: {
		_handleLinks: function() {
			arq_links = $('a[href*="arquivo.phtml?a="]');
			arq_links.each(function(link){
				href = $(arq_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/arquivo/'+href[1];
				$(arq_links[link]).attr('href',nhref);
			});

			art_links = $('a[href*="artigo.phtml?a="]');

			art_links.each(function(link){
				href = $(art_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/artigo/'+href[1];
				$(art_links[link]).attr('href',nhref);
			});
			
			
			art_links = $('a[href*="artigo.phtml/"]');
			
			art_links.each(function(link){
				href = $(art_links[link]).attr('href');
				href = href.split('/');
				nhref = '#/artigo/'+href[href.length-1];
				$(art_links[link]).attr('href',nhref);
			});
		},
		_view: function() {
			this.Noticia.get(
				{
					id: this.$routeParams.id
				}
			).$promise.
			then(function(data){
				this.$.noticia = data.data;
				setTimeout(function(){
					this._handleLinks();
				}.bind(this), 1000);
			}.bind(this));
		}
	}
});
