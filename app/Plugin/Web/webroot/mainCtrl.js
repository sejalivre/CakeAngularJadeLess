fenapaesApp.cC({
	name: 'mainController', 
	inject: ['$scope','$resource'],
	init: function() {

		this.$.version = '3.3.2';
		this.$.theme = 'lumen';
		this.$.message = 'Bem-vindo';
		
	},
	methods: {
		go: function(where) {
			location.href = where;
		}
	}
});
