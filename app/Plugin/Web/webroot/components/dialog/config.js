fenapaesApp.config(function($translateProvider){
	/**
	* These are the defaults set by the dialogs.main module when Angular-Translate
	* is not loaded.  You can reset them in your module's configuration
	* function by using $translateProvider.  If you want to use these when
	* Angular-Translate is used and loaded, then you need to also load
	* dialogs-default-translations.js or include them where you are setting
	* translations in your module.  These were separated out when Angular-Translate
	* is loaded so as not to clobber translation set elsewhere in one's 
	* application.
	*/
	$translateProvider.translations('pt-BR',{
		DIALOGS_ERROR: "Erro",
		DIALOGS_ERROR_MSG: "Ocorreu um erro desconhecido.",
		DIALOGS_CLOSE: "Fechar",
		DIALOGS_PLEASE_WAIT: "Espere",
		DIALOGS_PLEASE_WAIT_ELIPS: "Espere...",
		DIALOGS_PLEASE_WAIT_MSG: "Esperando a operação completar.",
		DIALOGS_PERCENT_COMPLETE: "% Completa",
		DIALOGS_NOTIFICATION: "Notificação",
		DIALOGS_NOTIFICATION_MSG: "Notificação de aplicacão desconhecida.",
		DIALOGS_CONFIRMATION: "Confirmação",
		DIALOGS_CONFIRMATION_MSG: "Confirmação requirida.",
		DIALOGS_OK: "OK",
		DIALOGS_YES: "Sim",
		DIALOGS_NO: "Não"
	});

});