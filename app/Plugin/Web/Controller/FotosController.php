<?php
	class FotosController extends WebAppController {
		
		public function view($id = null, $size = 'm') {
			
			$this->layout = 'image';
			
			$image = @imagecreatefromjpeg('http://apaebrasil.org.br/foto.phtml/'.$id.'/'.$size);
			if (!$image) {
				$image = @imagecreatefromgif('http://apaebrasil.org.br/foto.phtml/'.$id.'/'.$size);				
			}
			if (!$image) {
				$image = @imagecreatefrompng('http://apaebrasil.org.br/foto.phtml/'.$id.'/'.$size);				
			}
			header('Content-type:image/png');
			
			echo imagejpeg($image);
			
			$this->render(false);
			
		}
		
	}