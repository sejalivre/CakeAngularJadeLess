<?php
	class UsersController extends WebAppController {
		
		public function login() {
			$this->layout = 'Web.login';
			
			if ($this->request->is('post')) {
				$data = $this->request->data;
				
				if ($this->Auth->login()){
					$this->redirect('/');
				}
			}
		}
		
		public function logout() {
			$this->Auth->logout();
			$this->redirect('/');
		}
		
	}