<?php
	class tbsHelper extends AppHelper {
		
		var $helpers = array('Html');
		
		public function linkbtn($options = array()) {
			$defaults = array(
				'text' => 'Link'
			);
			$options = array_merge($defaults, $options);
			return '<a href="btn btn-default">'.$options['text'].'</a>';
		}
		
		public function jqJS() {
			echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js');
		}
		
		public function tbsCSS() {
			echo $this->Html->css('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css');
		}
		public function tbwCSS() {
			echo $this->Html->css('/admin/bower_components/bootswatch/lumen/bootstrap.min.css', array('id'=>'BWTheme'));
		}
		
		public function tbsJS() {
			echo $this->Html->script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js');
		}
		
	}