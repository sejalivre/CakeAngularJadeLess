<?php
	class AdminAppController extends AppController {
		
		var $helpers = array(
			'Bootstrap.tbs',
			'Angular.angular',
			'Html',
			'Form'
		);
		var $layout = 'Admin.theme';
		
		public function beforeFilter() {
			
		}
		
		public function index() {
			
		}
		
		public function add() {
			
		}
		
		public function edit($id = null) {
			
		}
		
		public function del($id = null) {

		}

	}