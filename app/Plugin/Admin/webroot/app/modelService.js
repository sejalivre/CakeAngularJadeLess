sitesApp.service('ModelService', ['$resource','DataService', function($resource, DataService) {
	this.model = {
		Noticia: {
			res: $resource('/api/noticia/:id.json'),
			params: {
				q: 'Noticia.not_ent_id.eq.'+DataService.Entidade.id,
				sort: '-not_data'
			},
			page: 1,
			get: function() {
				return this.model.Noticia.res.get(this.model.Noticia.params).$promise;
			}.bind(this),
		},
		Pagina: {
			res: $resource('/api/pagina/:id.json'),
		},
		Menu: {
			res: $resource('/api/menu/:id.json'),
			save: function(id, data) {
				this.model.Menu.res.save({id:id},data);
			}.bind(this)
		},
		Arquivo: {
			res: $resource('/api/arquivo/:id.json'),
		},
		Banner: {
			res: $resource('/api/banner/:id.json'),
		},
		Foto: {
			res: $resource('/api/foto/:id.json'),
		},
		Bootswatch: {
			res: $resource('http://api.bootswatch.com/3/'),
			get: function() {
				return this.model.Bootswatch.res.get().$promise;
			}.bind(this),
		},
	};

}]);