sitesApp.cC({
	name: 'noticiaFormCtrl',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		if (this.$routeParams.not_id) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
	},
	methods: {
		_add: function(){
			this.$.header = 'Nova Notícia';
		},
		_edit: function() {
			this.$.header = 'Editar Notícia';
		}
	}
});