sitesApp.cC({
	name: 'bannerCtrl',
	inject: ['$scope','$resource'],
	init: function() {
		this.Banner = this.$resource('/api/banner/:id.json');
		this._load();
	},
	methods: {
		_load: function() {
			this.Banner.get(
				{
					q: 'Banner.ban_ent_id.eq.1',
					populate: 'BannerImg'
				}
			).$promise
			.then(function(data){
				this.$.Banners = data.data;
			}.bind(this));
		}
	}
});
